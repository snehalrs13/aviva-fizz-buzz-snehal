package com.aviva.service;



import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aviva.request.FizzrRequest;
import com.aviva.response.FizzResponse;

@RestController
public class FizzBuzz {

	@RequestMapping("/fizzbuzz")
	public FizzResponse printString(@RequestParam(value="FizzRequest")FizzrRequest request)
	{
		Date date = new Date();
		
		FizzResponse fizzResponse =new FizzResponse();
		String msg=null;
		try {
		if(request!=null)
		{
			
			if(request.getNumber()>0)
			{
				//number between 1 and 1000
				if(request.getNumber()>1 && request.getNumber()<1000)
				{
				for(int i=1;i<request.getNumber();i++)
				{
					//print number between 1 and entered number
					System.out.println("Number:" +i);
					
					//number is divisible by 3 AND 5 ,print fizz buzz
					if((request.getNumber()%3==0) &&(request.getNumber()%5==0))
					{
						//if today is Wednesday,print wizz wuzz
						if(date.getDay()==3)
							msg= "wizz wuzz";
							else
						    msg="fizz buzz";
						System.out.println(msg);
					
					}
					else if(request.getNumber()%3==0)//number is divisible by 3 , print fizz
					{
						//if today is Wednesday,print wizz 
						if(date.getDay()==3)
							msg= "wizz";
						else
							msg="fizz";
						System.out.println(msg);
					}
					else if(request.getNumber()%5==0) //number is divisible by 5, print buzz
					{
						//if today is Wednesday,print wuzz
						if(date.getDay()==3)
							msg= "wuzz";
						else
						msg="buzz";
						System.out.println(msg);
					}
					else
						msg="Number is not divisible by 3 and 5";
					
					
				}
				}
			}
		}
		fizzResponse.setMessage(msg);
		fizzResponse.setStatuscode(200);
		}
		catch(Exception e)
		{
			fizzResponse.setMessage(e.getMessage());
			
			e.printStackTrace();
		}
		
		return fizzResponse;
	}
	
	
	
	
}
